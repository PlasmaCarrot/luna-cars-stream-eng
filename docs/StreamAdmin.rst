Administration Manual
=================================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           |**Description**                                                     |
+===================+====================================================================+
|Best shot          |A frame of a video stream on which a vehicle or a license plate is  |
|                   |                                                                    |
|                   |detected in the optimal perspective for further processing.         |
+-------------------+--------------------------------------------------------------------+
|Detection          |Actions to determine areas of the image that contain vehicle or LP. |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+
|TrackEngine        |TrackEngine is library that used to track the position of the       |
|                   |                                                                    |
|                   |vehicle or LP in the sequence of frames and select the best frame.  |
+-------------------+--------------------------------------------------------------------+

Introduction
----------------

This document is manual for administrator of the CARS Stream 1.0.6 service.

This manual describes the installation, configuration and administration of the service.

It is recommended to carefully read this manual before installing and using the service.

General Information
-----------------------

**VisionLabs LUNA CARS** – VisionLabs LUNA CARS is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS Analytics, CARS API and CARS Stream.

**VisionLabs LUNA CARS Stream** – designed for detection and tracking of vehicles and LP in a video stream or detection in images. The main functions of the service are presented below:

* Video stream processing;

* Detection and tracking of vehicles and license plates;

* Choosing the best shot;

* Display of results of detection and recognition.

System Requirements
--------------------------

Several requirements and conditions must be considered when preparing the installation and operation of the CARS Stream software, as indicated below. The list of system requirements is presented in Table 1.

**Table 1**. System Requirements

+---------------------------+-----------------------------------------------------------+
|**Resources**              |**Recommended**                                            |
+===========================+===========================================================+
|CPU                        |Intel 4 Cores 2,0 GHz                                      |                          
+---------------------------+-----------------------------------------------------------+
|RAM                        |8 GB or higher                                             |         
+---------------------------+-----------------------------------------------------------+
|HDD or SSD                 |20 GB or higher                                            |
+---------------------------+-----------------------------------------------------------+
|OS                         |CentOS 7.4 x86_64                                          |                       
+---------------------------+-----------------------------------------------------------+
|Supported instructions     |AVX 2                                                      |                       
+---------------------------+-----------------------------------------------------------+
|Software                   |Python 3.6;                                                |
|                           |                                                           |    
|                           |PostgreSQL 9.6;                                            |    
|                           |                                                           |    
|                           |postgreSQL-libs;                                           |    
|                           |                                                           |    
|                           |GCC 8.1.                                                   |    
+---------------------------+-----------------------------------------------------------+
|Browser                    |Microsoft Edge (44.0 and up);                              |
|                           |                                                           |    
|                           |Mozilla Firefox (60.3.0 and up);                           |    
|                           |                                                           |    
|                           |Google Chrome (50.0 and up).                               |    
+---------------------------+-----------------------------------------------------------+

Several factors affect system requirements:

* The number of processed video streams;

* Frequency and resolution of frames of video streams;

* CARS Stream settings. The default settings are the most versatile. Depending on the operating conditions of the application, setting values can affect quality or performance.

CARS Stream can also work in the mode of accelerating calculations per account:

* Using AVX instructions (automatically detected during installation).

1. Installation
----------------------

1.1. Pre-install Process
''''''''''''''''''''''''''''''''

The CARS Stream distribution is a «carstream_linux_v.*.zip» archive.

The archive contains the components necessary for the installation and operation of the service. The archive does not include some dependencies that are included in the standard distribution of the CentOS repository and can be downloaded from open sources during the installation process.

.. note:: Entire configuration and installation process must be performed under a superuser account (with root rights).

1.2. Parameters of «TrackEngine.conf» Configuration File
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

This section describes the parameters of the «TrackEngine.conf» configuration file that are used to set up CARS Stream. This file is in CARS Stream distribution in the /bin/data directory.

1.2.1. Detector-step
""""""""""""""""""""""""""

The «detector-step» parameter allows you to specify the frequency with which the vehicle and the LP are detected in the specified area before the detection is performed (redetection). Redetection requires fewer resources, but the object can be lost when specifying many frames. ::

    <!-- detector-step: The count of frames between frames with full detection,
    [0 .. 30] ('7' by default). -->
    <param name="detector-step" type="Value::Int1" x="7" />

1.2.2. Detector-scaling
""""""""""""""""""""""""""""

The «detector-scaling» parameter allows you to scale the frame before processing. Frame scaling is necessary to reduce the frame size, and therefore reduce the processing time. ::

    <!-- detector-scaling: Scale frame before detection for performance reasons,
    [0, 1] ('0' by default). -->
    <param name="detector-scaling" type="Value::Int1" x="0" />

1.2.3. Scale-result-size
""""""""""""""""""""""""""""""

A suitable frame size for work should be selected using the «scale-result-size» parameter. Parameter sets the maximum frame size after scaling to the largest of the frame sides. If the original frame was 1920x1080 and the value of «scale-result-size» is 640, then CARS Stream will process a 640x360 frame.

The lower the frame resolution, the less system resources are consumed. ::

    <!-- scale-result-size: If scaling is enabled, frame will be scaled to this
    size in pixels (by the max dimension - width or height).
    Upper scaling is not possible. ('640 by default') -->
    <param name="scale-result-size" type="Value::Int1" x="640" />

.. note:: If the frame was cropped using the «roi» parameter, then scaling will be applied to this reduced frame. In this case, the «scale-result-size» value should be set depending on the largest side of cropped area.

1.2.4. Frg-subtractor
""""""""""""""""""""""""""

When the «frg-subtractor» parameter is enabled, the mode of accounting for movements in the frame is enabled. Subsequent object detection will not be performed on the entire frame (only in areas with movement).

Enabling the frg-subtractor parameter increases the performance of CARS Stream. ::

    <!-- frg-subtractor: Use foreground subtractor for filter of frames, [0, 1]
    ('1' by default). -->
    <param name="frg-subtractor" type="Value::Int1" x="1" />

1.2.5. Frg-regions-alignment
""""""""""""""""""""""""""""""""

The «frg-regions-alignment» parameter allows you to set the alignment for areas with movement. ::

    <!-- frg-regions-alignment: frg regions alignment. Useful for detector
    better batching utilization. -->
    <!-- 0 or negative values mean using non aligned regions, (0 by default).
    -->
    <param name="frg-regions-alignment" type="Value::Int1" x="0" />

1.2.6. Frg-regions-square-alignment
""""""""""""""""""""""""""""""""""""""

When the «frg-regions-square-alignment» parameter is enabled, the width and height of the region with movement will always be equal. ::

    <!-- align frg regions to rect with equal sides (max side chosen). See frgregions-
    alignment, [0, 1] ('1' by default). -->
    <param name="frg-regions-square-alignment" type="Value::Int1" x="1" />

1.2.7. Batched-processing
""""""""""""""""""""""""""""""""""""""

The «batched-processing» parameter enables batch processing of frames. When working with several cameras, a frame is collected from each camera. This batch of frames is processed. If the parameter is disabled, then frames are processed one after another.

Batch processing increases the delay before processing, but the processing itself is faster. ::

    <!-- batched-processing: Process streams frames in batch or separately, [0,
    1] ('1' by default). -->
    <param name="batched-processing" type="Value::Int1" x="1" />

1.2.8. Min-frames-batch-size
""""""""""""""""""""""""""""""""""

The «min-frames-batch-size» parameter sets the minimum number of frames that should be accumulated from all cameras before processing.

    <!-- min-frames-batch-size: stream frames min batch size value to process, (
    '0' by default). -->
    <!-- higher values lead to higher processing latency but increase throughput
    and device utilization. -->
    <!-- zero/negative values disable this feature, so any stream frames will be
    processed if they are available -->
    <!-- note: this parameter should be regulated with 'max-frames-batch-gathertimeout'
    (see below) -->
    <param name="min-frames-batch-size" type="Value::Int1" x="0" />

.. note:: It is recommended to set the value of the «min-frames-batch-size» parameter equal to the number of connected threads when using a GPU. It is recommended to set the value of the «min-frames-batch-size» parameter to «2» when using the CPU.

1.2.9. Max-frames-batch-gather-timeout
""""""""""""""""""""""""""""""""""""""""""

The «max-frames-batch-gather-timeout» parameter sets the time between processing of frame batch. If the processing of one frame falls within the specified time and there is additional buffer, then CARS Stream waits for additional frames to increase GPU utilization.

If «max-frames-batch-gather-timeout» is 20 ms, then during this time the previous batch is processed and a new batch is assembled. After 20 ms, the next processing starts, even if it was not possible to collect the number of frames equal to «min-frames-batch-size». The next frame processing cannot start until the processing of the previous batch of frames is finished.

If it is equal to «0», then there is no delay for filling the batch, but processing occurs continuously and «min-frames-batch-size» is ignored. ::

    <!-- max-frames-batch-gather-timeout: max available timeout to gather next
    stream frames batch (see 'min-frames-batch-size') from last processing
    begin time point (measured in ms), ('-1' by default). -->
    <!-- negative values disable this feature (no timeout, so only stream frames
    batches with size no less than 'min-frames-batch-size' value will be
    processed) -->
    <!-- note: this parameter is complementary to 'min-frames-batch-size' and
    controls min average fps of stream frames batches processing -->
    <param name="max-frames-batch-gather-timeout" type="Value::Int1" x="-1" />

.. note:: It is recommended to set the value of the max-frames-batch-gather-timeout parameter to «0» both when using a GPU and when using a CPU.

1.3. Installation with Ansible
''''''''''''''''''''''''''''''''
You need to install the Ansible package by running the commands.

Package manager update: ::

    yum update

Additional repositories install: ::

    yum install epel-release

Ansible installation: ::

    yum install ansible

1.3.1. Settings SSH
""""""""""""""""""""""""""""""""""""""""""""""""""

On the source server, you need to generate an SSH-key and add it to the target server. First you need to check and configure the SSH-service: ::

    systemctl status sshd

Run servise if SSH ended: ::

    systemctl start sshd

Install server if needed: ::
    
    yum install -y openssh-server

After that, you need to generate a key by running the command: ::

    ssh-keygen

If necessary, you can specify a passphrase or leave it blank. To copy the key to the target server, enter the command: ::

    ssh-copy-id username@hostname

«username» is the name of the authorized user and «hostname» is the IP-address of the target server.

.. note:: This method is not the only one possible. You can use any other convenient method to provide SSH bridge between the installer server and the target server.

1.3.2. Settings of «hosts» Configuration File
""""""""""""""""""""""""""""""""""""""""""""""""""

The archive contains the «hosts» file. This file is in the /ansible directory. You need to set the external IP-address of the server where the application will be installed.

    #CARS Stream
    #Multiple hosts allowed
    [stream]
    <IP_adress>

1.3.3. Settings of «all.yml» Configuration File
""""""""""""""""""""""""""""""""""""""""""""""""""""""

It is necessary to configure the service in the «all.yml» configuration file located in the /ansible/group_vars directory. Description of the parameters is in the Table 2.

**Table 2**. Parameters of «all.yml» configuration file 

+---+----------------------+----------------------------------------------------------------+
|#  |**Parameter**         | **Description**                                                |
+===+======================+================================================================+
|1  |luna_cs_vers          |Specifies name of the archive. For example, carstream_linux_v.*.|
+---+----------------------+----------------------------------------------------------------+
|2  |luna_cs_zip_location  |This parameter sets the path to the CARS Stream archive. For    |
|   |                      |                                                                |
|   |                      |example, /distr/api/carstream_linux_v.*.                        |
+---+----------------------+----------------------------------------------------------------+

1.3.4. Starting Installation with Ansible
"""""""""""""""""""""""""""""""""""""""""""""

To start the installation, you must be in /ansible directory and execute the following command: ::

    ansible-playbook -i hosts install_stream.yml

.. note:: During the installation process, the built-in firewall and selinux will be disabled (a reboot will be required later).

1.3.5. Service Check
"""""""""""""""""""""""""""""""""""""

After installation, it is possible to check the status of the CARS Stream service with the command: ::

    systemctl status luna-cars-stream

If CARS Stream works correctly, the system should not display any error messages.

1.4. Installation with Docker
''''''''''''''''''''''''''''''''''''''''

1.4.1. Installation Docker and Docker-compose
""""""""""""""""""""""""""""""""""""""""""""""

You can use the official `instruction <https://docs.docker.com/engine/install/centos/>`_ for CentOS, as this instruction is the most up to date.

Additional dependencies: ::

    sudo yum install -y yum-utils

Add docker repository: ::

    sudo yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo

Docker installations: ::

    sudo yum install docker-ce docker-ce-cli containerd.io

Installation check: ::

    docker -v

Download docker-compose: ::

    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

Assigning an Executable File Property: ::
    
    sudo chmod +x /usr/local/bin/docker-compose

1.4.2. Settings of «.env» File
""""""""""""""""""""""""""""""""""""""""""""""

Description of the parameters is in the Table 3.

**Table 3**. Parameters of «.env» file

+---+-------------------+-------------------------------------------------------------------+
|#  |**Parameter**      | **Description**                                                   |
+===+===================+===================================================================+
|1  |HASP_license_server|Specifies the path to the server to which the installer calls for  |
|   |                   |                                                                   |
|   |                   |an available network product license. If there is no network       |
|   |                   |                                                                   |
|   |                   |license, then it must be specified locally.                        |
+---+-------------------+-------------------------------------------------------------------+
|2  |HASP_wait_time     |Specifies the time to response when a request for an available     |
|   |                   |                                                                   |
|   |                   |license is sent to the server. Set in minutes.                     |
+---+-------------------+-------------------------------------------------------------------+
|3  |Emirates           |Selecting country for license plate recognition. Possible values   |
|   |                   |                                                                   |
|   |                   |* true – system will recognize only UAE license plate;             |
|   |                   |                                                                   |
|   |                   |* false – system will recognize license plates of RF, CIS and EU.  |
+---+-------------------+-------------------------------------------------------------------+
|4  |ENG                |Specifies service language. Possible values:                       |
|   |                   |                                                                   |
|   |                   |* true – English UI;                                               |
|   |                   |                                                                   |
|   |                   |* false – Russian UI.                                              |
+---+-------------------+-------------------------------------------------------------------+

1.4.3. Starting Installation with Docker
""""""""""""""""""""""""""""""""""""""""""""""

In folder (where docker-compose.yml is located) run the command: ::
    
    docker -compose up –d

After changing «.env» file or other parameters, you need to start service with the rebuild key: ::

    docker -compose up -d –build

1.5. CARS Stream Management
''''''''''''''''''''''''''''''''

Use the following commands to manage the service: 

System start: ::

    sudo systemctl start luna-cars-stream

System reboot: ::

    sudo systemctl restart luna-cars-stream

Check status: ::

    sudo systemctl status luna-cars-stream

Update system: ::

    sudo systemctl stop luna-cars-stream

The process of adding cameras is described in the "CarStreamServerApi.html" file, which is located in the CARS Stream archive in the / doc directory.

2. Detection Area Configuration
-----------------------------------

Detection area is a rectangle area of the frame, inside which the vehicle parameters are extracted. Detection area is used to capture recognized vehicles (events) passing through this area of the frame.

The detection zone is configured in «input_roi_config.ini» file located in the /stream/bin/data directory. Description of file parameters is presented in Table 4.

**Table 4**. Configuration file parameters

+---+------------------+--------------------------------------------------------------------+
|#  |**Parameter**     | **Description**                                                    |
+===+==================+====================================================================+
|1  |camera_name       |Camera name. Detection area will be configured for this camera.     |
+---+------------------+--------------------------------------------------------------------+
|2  |min_height        |The value of the upper border of the detection zone.                |
+---+------------------+--------------------------------------------------------------------+
|3  |max_height        |The value of the lower border of the detection zone.                |
+---+------------------+--------------------------------------------------------------------+
|4  |min_width         |The value of the left border of the detection zone.                 |
+---+------------------+--------------------------------------------------------------------+
|5  |max_width         |The value of the right border of the detection zone.                |
+---+------------------+--------------------------------------------------------------------+

.. note:: All border values are specified in pixels. The coordinate system is located in the upper left corner of the frame.

After applying changes, you need to restart the CARS Stream service: ::

    sudo systemctl restart luna-cars-stream

.. note:: If you plan to use CARS Stream in conjunction with CARS Analytics, then ROI and DROI zones are recommended to configure in the web-interface.

3. Visual Mode of CARS Stream
-----------------------------------

CARS Stream allows you to view video streams.

The list of video streams is located: ::

    http://<IP_address>:34569/api/1/streams.

The web-browser provides information about video streams

::

    [{"alive":1,"health_check":{"max_error_count":10,"period":3600,"retry_delay":5},"id":"fd69115d-3274-468b-8fcc-8dcd1f12c1e4",
    "input":{"roi":[356,148,1896,1045],
    "rotation":0,
    "transport":"tcp","url":"rtsp://admin:Seven182@11.12.82.113:574/cam/realmonitor?channel=1&subtype=0"},
    "name":"StreetView","output":{"token":"","url":""},
    "preview_url":"/api/1/streams/preview/fd69115d-3274-468b-8fcc-8dcd1f12c1e4",
    "video_info":{"bit_rate":0,"frame_rate":25,"gop_size":12,"height":1296,"start_time":"2021-08-09T16:53:17 MSK","width":2304}}]

To view video stream, copy the value of the «preview_url» parameter and add it to the CARS Stream address: http://<IP_address>:34569/api/1/streams/preview/fd69115d-3274-468b-8fcc-8dcd1f12c1e4.

This opens a window in the browser (Figure 1), where the system detects and tracks the vehicle in the detection area in real time in the video stream, and on the right, the vehicle's recognized data (track number, model, brand, category) and license plate (image and symbols recognized by the system). Frame boundaries are determined by the settings of the detection area (section 2).

Viewing the processed video is used to debug the camera.

4. Logging
----------------------

Log files are saved in /var/lib/luna/cars/stream/bin/log, you should use any convenient text editing method to view.

The logging level is configured in the file /bin/data/csConfig.conf in the Logging section.

4.1. Severity
''''''''''''''''''''''''

Severity is parameter that defines the information that the user wants to receive in the logs. The following information filters are available: 

* 0 – All information;

* 1 – Only warnings;

* 2 – Only errors.

::

    "severity": 
    {
	    "value": 1,
	    "description" : "Logging severity levels …"
    }

4.2. Mode
''''''''''''''''''''

Mode is parameter that sets the application logging mode: file or console. There are three modes:

* l2c – show info only in console;

* l2f – show info only in file;

* l2b – show info in console and file.

::

    "mode": 
    {
	    "value": "l2b",
	    "description": " Mode of logging … "
    }

5. Licensing
---------------------------

LUNA CARS uses the HASP license utility.

The system can operate in 2 modes – local and server. In the local mode, HASP is installed on each machine separately, in the server mode – only on the server.

5.1. HASP Installation and Configuration
'''''''''''''''''''''''''''''''''''''''''''''''''

Before obtaining a license, you must install the HASP utility on the server. Move to the directory with the utility distribution /stream/extras/hasp and run the HASP installation. ::

    yum install -y haspd-7.60-vlabs.x86_64.rpm

After installation, you need to start HASP with the following commands: ::

    systemctl daemon-reload 
    systemctl start aksusbd 
    systemctl enable aksusbd

You can check the status of the utility using the command: ::

    systemctl status aksusbd

If the utility is successfully launched, the result of the executed command will be the following message: :: 

    aksusbd.service - LSB: Sentinel LDK RTE
        Loaded: loaded (/etc/rc.d/init.d/aksusbd; bad; vendor preset: disabled)
        Active: active (running) since Tue 2021-06-29 16:32:43 MSK; 1 day 19h ago
         Docs: man:systemd-sysv-generator(8)
        CGroup: /system.slice/aksusbd.service
           ├─909 /usr/sbin/aksusbd
           ├─920 /usr/sbin/winehasp
           └─953 /usr/sbin/hasplmd -s

5.2. License Activation
'''''''''''''''''''''''''''''''''''''

A system fingerprint file is required to obtain a license. It needs to be generated in the / hasp directory using the following script: ::

    ./LicenseAssist fingerprint <file_name>.c2v

In some cases, an access error may occur. It is necessary to grant permissions to the script and repeat the process: ::

    chmod +x LicenseAssist

After the process of generating a system fingerprint is completed, it will need to added fingerprint to request for technical support.

Technical support staff will send an email with a license file in «.v2c» format. This file will need to be uploaded to the server with the HASP utility installed.

The license file must be added to HASP via the web interface. By default, when the HASP utility is launched, the interface is available at the following address: http://<IP_address>:1947/_int_/checkin.html.

On the «Update/Attach» page, you need to add the license file and apply the changes (Figure 2).

After applying file, the system should display a message about the successful completion of the operation.