User Manual
=========================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           |**Description**                                                     |
+===================+====================================================================+
|Bbox (Bounding box)|Rectangle that restricts the image space with the detected object   |
|                   |                                                                    |
|                   |(vehicle or license plate).                                         |
+-------------------+--------------------------------------------------------------------+
|Best shot          |A frame of a video stream on which a vehicle or a license plate is  |
|                   |                                                                    |
|                   |detected in the optimal perspective for further processing.         |
+-------------------+--------------------------------------------------------------------+
|Classifier         |A system parameter that recognizes one of vehicle or LP attributes. |
+-------------------+--------------------------------------------------------------------+
|EXIF               |A format that allows you to add additional information (metadata) to|
|                   |                                                                    |
|                   | images, commenting on this file, describing the conditions and     |
|                   |                                                                    |
|                   |methods of obtaining it, authorship, etc.                           |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+

Introduction
-----------------

This document is manual for user of the CARS Stream 1.0.6 service.

The manual defines how the user operates in CARS Stream service.

It is recommended to carefully read this manual before using the service.

General information
--------------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS Analytics, CARS API and CARS Stream.

**VisionLabs LUNA CARS Stream** designed for detection and tracking of vehicles and LP in a video stream or detection in images. The main functions of the service are presented below:

* Video stream processing;

* Detection and tracking of vehicles and license plates;

* Choosing the best shot;

* Display of results of detection and recognition.

System Requirements
-------------------------

Image Requirements
'''''''''''''''''''''''''''''''''

Requirements for incoming images of vehicles and LP:

* Images must be three-channel (RGB) or black and white;

* Image format: JPEG encoded in the Base64 standard;

* Images should not contain EXIF tags;

* The angle of shooting of the vehicle and the LP can be any except for «vertical», when the camera is above the object;

* Vehicle and license plate must be fully visible on the frame;

* Supported image size from 320x240 to 1920x1080 px.

Video Requirements
'''''''''''''''''''''''''''''''''''

Requirements for incoming video of vehicles and LP:

* Recommended resolution - 1920x1080 px;

* The frame rate must be constant;

* Supported bitrate - 4096 Kb/s;

* Shutter speed (exposure) - no more than 1/200 ;

* Used data transfer protocols - TCP, RTCP.

.. note:: This set of parameters (except for exposure) is the recommended minimum at which the system works efficiently. Decreasing the values may also reduce the number of detections, while increasing the value may unnecessarily load the system.

(*) The shutter speed should be selected based on the speed of traffic. The higher the speed of traffic, the faster the shutter should operate.

1. CARS Stream Configuration
-------------------------------

The CARS Stream parameters are configured using the /bin/data/csConfig.conf file.

1.1. Best-shot-observer
''''''''''''''''''''''''''''''

The method of processing the best shots of vehicles and license plates. ::

  "best-shot-observer": 
  {
	  "value": "car-plate",
	  "description": "Best shot observer type. ['car-plate' - to use car and plate best shot observer.]"
  },

1.2. Car-recognition
''''''''''''''''''''''''''''

Section with settings for connecting to CARS API and recognition thresholds required to display data in the preview window. 

1.2.1. Mode
"""""""""""""""""""""""""""""
Mode 'luna-cars' is used CARS API for vehicle and license plate recognition. ::

    "mode": {
	  "value": "luna-cars",
	  "description": "Mode of car recognition, ['luna-cars' - to use luna-cars service]"
  },

1.2.2. Luna-cars
""""""""""""""""""""""""""""

Settings for interaction with CARS API. To recognize vehicles and license plates from one video stream, you must have at least two CARS API services balanced using nginx.

1.2.2.1. Protocol 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Protocol for interaction with the CARS API module.

Default value: http. ::

  "protocol": {
      "value": "http",
      "description": "Data transfer protocol, ['http', 'https'], ('http' by default)." },

1.2.2.2. Host
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Address to NGINX balancer, which distributes the load between CARS API services. ::

  "host": {
      "value": "127.0.0.1",
      "description": "Luna-cars service address. ('127.0.0.1' by default)." 
  },

1.2.2.3. Port
^^^^^^^^^^^^^^^^^^^^^^^

Port to nginx balancer, which distributes the load between CARS API services. ::

  "port": {
      "value": 8082,
      "description": "Luna-cars service port, ('8082' by default)" 
  },

1.2.2.4. Max-retries
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The number of connection attempts before displaying an error message. 

Default value: 5; ::

  "max-retries": {
      "value": 5,
      "description": "Specifies maximum number of retries before giving up. ('5' by default)."
  },

1.2.2.5. Classifiers
^^^^^^^^^^^^^^^^^^^^^^^^

One or more classifiers can be specified as a parameter value. The classifiers determine what data about the vehicle and the license plate should be obtained. Description of classifiers is given in the Table 1. ::

  "classifiers": {
				"value": ["grz_all_countries", "car_brand_model", "vehicle_type", "vehicle_color", "vehicle_descriptor"],
				"description": "Array of luna cars classifiers, '['grz_all_countries', 'car_brand_model', 'pmt_bad_photo', 'pmt_grz_quality', 'marka_taxi_mt', 'vehicle_type', 'vehicle_color', 'vehicle_descriptor']', ('['grz_all_countries', 'car_brand_model', 'vehicle_type', 'vehicle_color', 'vehicle_descriptor']' by default)"
	}

**Table 1**. Description of classifiers

+------------------------------+-------------------------------------------------------+
|**Field Name**                |**Description**                                        |
+==============================+=======================================================+
|grz_ai_recognition            |LP recognition. Returns the recognized LP characters   |
|                              |                                                       |
|                              |(series, registration number and registration region   |
|                              |                                                       |
|                              |code) and an estimate of the recognition accuracy of   |
|                              |                                                       |
|                              |each of the LP characters.                             |
|                              |                                                       |  
|                              |Displays letters of the Latin and Russian alphabets.   |
+------------------------------+-------------------------------------------------------+
|marka_taxi_mt                 |Returns the brand of the vehicle and its belonging to  |
|                              |                                                       |
|                              |the public transport.                                  |
+------------------------------+-------------------------------------------------------+
|pmt_grz_quality               |Evaluates the image quality of the license plate.      | 
+------------------------------+-------------------------------------------------------+
|pmt_bad_photo                 |Evaluates the image quality of the vehicle.            |
+------------------------------+-------------------------------------------------------+
|ts_bcd_type                   |Defines the vehicle category.                          | 
+------------------------------+-------------------------------------------------------+
|solid_line_intersection       |Determines if the car has violated the prohibition on  |
|                              |                                                       |
|                              |crossing a solid line.                                 |
+------------------------------+-------------------------------------------------------+
|speed_bad_good_spec           |In case of violation of the speed limit recorded by the|
|                              |                                                       |
|                              |camera, determines the conditions for issuing a fine.  |
|                              |                                                       |
|                              |A fine is not issued in following cases:               |
|                              |                                                       |
|                              | - vehicle belongs to a special vehicle;               |
|                              |                                                       |
|                              | - the image has a low quality.                        |  
+------------------------------+-------------------------------------------------------+
|car_brand_model               |Defines the brand and model of the vehicle.            |
+------------------------------+-------------------------------------------------------+
|vehicle_color                 |Determines the color of the vehicle.                   | 
+------------------------------+-------------------------------------------------------+
|vehicle_type                  |Defines type of vehicle.                               |
+------------------------------+-------------------------------------------------------+
|vehicle_descriptor            |Retrieves vehicle descriptor.                          | 
+------------------------------+-------------------------------------------------------+
|grz_country_recognition_v1    |Returns country of license plate registration.         |
+------------------------------+-------------------------------------------------------+
|eu_recognition_v1             |Returns results of the license plate recognition of EU.| 
+------------------------------+-------------------------------------------------------+
|rus_spec_recognition_v1       |Returns results of emergency vehicle recognition       |
|                              |                                                       |
|                              |numbers in Russia.                                     |
+------------------------------+-------------------------------------------------------+
|grz_bel_ukr_kzh_recognition_v1|It is used for recognizing the license plate of        |
|                              |                                                       |
|                              |Belarus, Ukraine, and Kazakhstan.                      |
+------------------------------+-------------------------------------------------------+
|grz_all_countries             |It is used to determine whether the license plate      |
|                              |                                                       |
|                              |belongs to a country, and then launches the recognition|
|                              |                                                       |
|                              |classifier for the corresponding country.              |
+------------------------------+-------------------------------------------------------+
|uae_recognition_v1            |The classifier is used to recognize the UAE LP.        | 
+------------------------------+-------------------------------------------------------+
|grz_emirate_recognition_v1    |The classifier returns the emirate of license plate.   |
+------------------------------+-------------------------------------------------------+
|grz_ai_recognition            |Used to determine whether a license plate belongs to a |
|                              |                                                       |
|                              |country, and then runs the recognition classifier for  |
|                              |                                                       |
|                              |the corresponding country. Unlike grz_all_countries, it|
|                              |                                                       |
|                              |does not display the name and accuracy of a specific   |
|                              |                                                       |
|                              |country.                                               |
+------------------------------+-------------------------------------------------------+
|vehicle_emergency_type        |Determines the type of emergency service based on the  |
|                              |                                                       |
|                              |vehicle image.                                         |
+------------------------------+-------------------------------------------------------+

1.2.3. Recognition-thresholds
""""""""""""""""""""""""""""""""""""""""

The parameters of this section define threshold values.

1.2.3.1. License-plate-score
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This classifier sets the threshold for the accuracy of the license plate recognition. If the accuracy of the license plate recognition obtained from CARS API is below the specified threshold value, then these recognition results are not considered when rendering. ::

  "license-plate-score": {
    "value": 0.5,
    "description": "License plate score threshold. [0.0 .. 1.0] ('0.5' by default)"
    }

1.2.3.2. License-plate-diff
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The «license-plate-diff» parameter is used when comparing the recognized license plate symbols from two different frames of the same track.

The value of the license-plate-diff parameter specifies the number of differing characters at which two license plates are still considered to belong to the same vehicle.

When operating, LP with the higher recognition accuracy will be selected from two license plates. ::

  "license-plates-diff": {
    "value": 1,
    "description": "Difference threshold between two compared license plates. [0..11] ('1' by default)" 
    }

1.2.3.3. License-plates-cmp-score
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The license-plates-cmp-score parameter sets the threshold for the accuracy of license plate recognition.

Two license plates for one track will be considered different if the number of differing characters is greater than the license-plate-diff threshold and the accuracy of their recognition is higher than the license-plates-cmp-score threshold. In this case, both license plates will be considered during rendering. ::

  "license-plates-cmp-scores": {
    "value": 0.9,
    "description": "Scores threshold of two compared license plates. [0.0 .. 1.0] ('0.9' by default)" 
    }

1.2.3.4. Brand-model-score
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The lower threshold for assessing the recognition accuracy of the vehicle model. If the score is lower, then these recognition results are not taken into account when rendering. ::

  "brand-model-score": 
    {
    "value": 0.5,
    "description": "Car brand model score threshold. [0.0 .. 1.0] ('0.5' by default)" 
    }

1.2.3.5. Vehicle-type-score 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The lower threshold for assessing the recognition accuracy of the vehicle category. If the obtained score value is below the specified threshold, then these recognition results are not considered when rendering. ::

  "vehicle-type-score": {
     "value": 0.9,
     "description": "Vehicle type score threshold. [0.0 .. 1.0] ('0.9' by default)"
  }

1.3. Analytics_callback
''''''''''''''''''''''''''''''''

Settings for interaction with CARS Analytics. ::

  # Connection protocol for CARS Analytics
  "protocol": {
            "value": "http",
            "description": "Data transfer protocol, ['http', 'https'], ('http' by default)."
        },
  # Address CARS Analytics
        "host": {
            "value" : "127.0.0.1",
            "description" : "Analytic backend host. ('127.0.0.1' by default)"
        },
  # Connection port
        "port": {
            "value" : 8000,
            "description" : "Analytic backend port. (8100 by default)"
        },
  # Logging level settings
        "logging": {
            "value" : 0,
            "description" : "Callback logging [0 - No callback logging, 1 - analytic response logging] (0 by default)"
        }

1.4. Logging
''''''''''''''''''''

Section with application logging settings. This section is responsible for displaying error messages or the current state of the application.

1.4.1. Severity
""""""""""""""""""""""""

Severity is parameter that defines the information that the user wants to receive in the logs. The following information filters are available: 

* 0 – All information, 

* 1 – Only warnings, 

* 2 – Only errors.

::

  "severity": 
  {
	  "value": 1,
	  "description" : "Logging severity levels …"
  }

1.4.2. Mode
""""""""""""""""""""

Mode is parameter that sets the application logging mode: file or console. There are three modes:

* l2c – show info only in console;

* l2f – show info only in file;

* l2b – show info in console and file.

::

  "mode": 
  {
	  "value": "l2b",
	  "description": " Mode of logging … "
  }

When outputting information to a file, you can configure the directory for saving logs using the launch parameter -Ld.

1.5. Debug
''''''''''''''''''

This section defines displaying information about detections in the video stream in the GUI. 

1.5.1. Show-window
""""""""""""""""""""""""""

Show-window setting allows to display the GUI-form of the stream from the camera and display the results of the detector operation on it.

.. note:: This setting is only available on GUI OS. You should run CARS Stream using the console in the GUI.

::

  "show-window":
  {
	  "value": false,
	  "description": "Show video window (false by default)."
  }

1.5.2. Frames-per-second
""""""""""""""""""""""""""""""

Frames-per-second (FPS) - the maximum number of frames per second for visualizing the application. If many frames per second is received for rendering, some frames will be skipped. The parameter affects the operation of the application only if the show-window variable is true. ::

  "frames-per-second" : {
	  "value" : 20,
	  "description" : "Maximum frames per second (all other will be skipped) during the visualization for each source (20 by default)."
  }

1.5.3. Data-lack-notification
""""""""""""""""""""""""""""""""""

Show information about the absence of data for the track in the GUI. For example, when the received recognition values for detections are lower than the thresholds specified in the «Recognition-thresholds» section. ::

  "data-lack-notification": {
     "value": false,
     "description": "Inform that there are no recognition results for track. Notification is showed on frame in recognition info block. It works for car and plate recognition only. ('false' by default)."
  }

2. Python Integration
--------------------------------

The application implements functionality for integration with third-party software in the Python language; for these purposes, there is a python-module «callback_manager.py». The data is processed by the «process_callback» (message) method. Message is of type dict and represents a set of recognition results in key-value format. Different objects have their own field descriptions. The description of the fields of the «vehicleData» object is given in Table 2.

**Table 2**. VehicleData fields description

+--------------------------+----------------------------------------------------+--------+
|**Field Name**            |**Description**                                     |**Type**|
+==========================+====================================================+========+
|                          |Vehicle metadata                                    |Dict    |
+--------------------------+----------------------------------------------------+--------+
|licensePlate              |Array of recognized LP. Recognizable characters     |String  |
|                          |                                                    |        |
|                          |include series, registration number and registration|        |
|                          |                                                    |        |
|                          |region code                                         |        |
+--------------------------+----------------------------------------------------+--------+
|licensePlateScore         |Assessment of the recognition accuracy of the LP    |Float   |
+--------------------------+----------------------------------------------------+--------+
|vehicleType               |Vehicle type. The following vehicle categories are  |String  |
|                          |                                                    |        |
|                          |available: A, B, C, D, E.                           |        |
+--------------------------+----------------------------------------------------+--------+
|vehicleTypeScore          |Assessment of the accuracy of determining the       |Float   |
|                          |                                                    |        |
|                          |vehicle category                                    |        |
+--------------------------+----------------------------------------------------+--------+
|carBrand                  |Vehicle brand                                       |String  |
+--------------------------+----------------------------------------------------+--------+
|carBrandModel             |Vehicle model                                       |String  |
+--------------------------+----------------------------------------------------+--------+
|carBrandModelScore        |Estimating the accuracy of determining the vehicle  |Float   |
|                          |                                                    |        |
|                          |model.                                              |        |
+--------------------------+----------------------------------------------------+--------+
|carImg (optional)         |Vehicle image in numpy array format                 |        |
+--------------------------+----------------------------------------------------+--------+
|licensePlateImg (optional)|Image of the vehicle LP in numpy array format       |        |
+--------------------------+----------------------------------------------------+--------+
|country                   |Vehicle country                                     |        |
+--------------------------+----------------------------------------------------+--------+
|countryscore              |Assessment of the recognition accuracy of the       |        |
|                          |                                                    |        |
|                          |vehicle country.                                    |        |
+--------------------------+----------------------------------------------------+--------+

The description of the detections object fields is given in Table 3.

**Table 3**. Detections fields descriptions

+--------------------------+----------------------------------------------------+-------+
|Field Name                |Description                                         |Type   |
+==========================+====================================================+=======+
|                          |List of vehicles bbox coordinates                   |       |
+--------------------------+----------------------------------------------------+-------+
|frameId                   |Vehicles bbox identifications                       |String |
+--------------------------+----------------------------------------------------+-------+
|x                         |Bbox x coordinate                                   |Int    |
+--------------------------+----------------------------------------------------+-------+
|y                         |Bbox y coordinate                                   |Int    |
+--------------------------+----------------------------------------------------+-------+
|width                     |Bbox width                                          |Int    |
+--------------------------+----------------------------------------------------+-------+
|height                    |Bbox height                                         |Int    |
+--------------------------+----------------------------------------------------+-------+

The description of the detections object fields is given in Table 4.

**Table 4**. Detections fields descriptions

+--------------------------+----------------------------------------------------+-------+
|Field Name                |Description                                         |Type   |
+==========================+====================================================+=======+
|recogn_update             |Recognition results for current track have arrived  |String |
+--------------------------+----------------------------------------------------+-------+
|track_end                 |Track end event                                     |String |
+--------------------------+----------------------------------------------------+-------+

Example of JSON respond: ::

  {
    'vehicleData' (dict): {
        'licensePlate' (str): 'H919CH40',
        'licensePlateScore' (double): 0.999257,
        'vehicleType' (str): 'C',
        'vehicleTypeScore' (double): 0.999257,
        'carBrand' (str): 'Kia',
        'carBrandModel' (str): 'Cerato',
        'carBrandModelScore' (double): 0.999257,
        'carImg' (numpy.ndarray, optional): [...],
        'licensePlateImg' (numpy.ndarray, optional): [...],
        'country' (str, optional): [...],
        'countryscore' (double, optional): [...],
    },
    'detections' (list(dict)): [{
        'frameId' (int): 12,
        'x' (int): 1282,
        'y' (int): 1390,
        'width' (int): 200,
        'height' (int): 120,
    }, ...],
    'operation' (str): 'recogn_update',
  }

3. Video-Stream Sources Settings
-----------------------------------------

CARS Stream supports simultaneous work with several video-stream sources. 

Configuration is set through the configuration file /var/lib/luna/cars/stream/bin/data/input.json. 

Several types of sources are supported:

* stream-sources – real time video sources. It can be both USB-cameras and IP-cameras (via RTSP);

* video-sources – video files;

* images-sources – set of frames in the form of separate image files.

All sources to be processed by the application are registered into the source configuration. The description of the configuration file parameters is given below. 

3.1. Name
''''''''''''''''

Source identification. It is used to identify the source of the sent frames. ::

  "name": "stream_0",

3.2. Input
''''''''''''''''''''''

3.2.1. Detection Area
""""""""""""""""""""""""""""""""""""

Detection area specifies the region of interest in which the object detection and tracking are performed (Figure 1). Detection are on the original frame is set in pixels as array [x, y width, height], where (x, y) are the coordinates of the upper-left point of the region of interest. Coordinate system is set in the same way as it is shown on the picture below.

When the values of width and height are set to «0», the entire frame will be the detection area. ::

  "roi": [0, 0, 0, 0],

3.2.2. Rotation
""""""""""""""""""""

The rotation angle of the image source. It is used when the incoming video-stream is rotated, for example, if the camera is installed on the ceiling. The value must be a multiple of 90. ::

  "rotation": 0,

3.2.3. Transport
""""""""""""""""""""""

The protocol of video stream transmission. The application can use one of two network protocols to receive video data: TCP or UDP. TCP is set in the application by default. 

.. note:: Transport is used only for stream-sources.

::

  "transport": "tcp",

TCP Protocol implements an error control mechanism that minimizes the loss of information and the skip of the reference frames at the cost of increasing the network delay. Key frames are the basis of various compression algorithms used in video codecs (for example, h264). Only the reference frames contain enough information to restore (decode) the image completely, while the intermediate frames contain only differences between adjacent reference frames. 

In terms of streaming, there is a risk of batch loss due to imperfect communication channels. In case of loss of the batch containing the data keyframe, the video-stream fragment cannot be correctly decoded. Consequently, distinctive artifacts appear, that are easily and visually distinguishable. These artifacts do not allow the face detector to operate in normal mode. 

The UDP protocol does not implement an error control mechanism, so the video-stream is not protected from damage. The use of this protocol is recommended only if there is a high-quality network infrastructure.

.. note:: With many video streams (10 or more), it is strongly recommended to use the UDP protocol. When using the TCP protocol, there may be problems with reading streams.

3.2.4. URL
"""""""""""""""""""""

Full path to the source or a USB-device number (for stream-sources): ::

  "url": "rtsp://stream_address"

Full path to the video file (for video-sources): ::

  "url": https://127.0.0.1/super_server/

Full path to the directory with the image: ::

  "url": "/example1/path/to/images/"
