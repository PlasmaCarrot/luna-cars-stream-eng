Release Notes
=====================

LUNA CARS Stream 1.0.6
--------------------------

* Updated vehicles and license plate detectors.

LUNA CARS Stream 1.0.5
-----------------------------

* Added support for «vehicle_emergency_type» classifier.

LUNA CARS Stream 1.0.4
----------------------------

* СARS.Analytics moved to a separate independent module: «car_stream_analytics» and «car_stream_db» removed.

* Added interaction with CARS.API v.0.0.10;

* Added support for obtaining information about the country from the «grz_all_countries» classifier.

* Added filtering of events by registration area on post-processing (ROI).

* Added saving of detection results to analytics using REST API.
