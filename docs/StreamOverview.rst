System Overview
=========================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           |**Description**                                                     |
+===================+====================================================================+
|Bbox (Bounding box)|Rectangle that restricts the image space with the detected object   |
|                   |                                                                    |
|                   |(vehicle or license plate).                                         |
+-------------------+--------------------------------------------------------------------+
|Best shot          |A frame of a video stream on which a vehicle or a license plate is  |
|                   |                                                                    |
|                   |detected in the optimal perspective for further processing.         |
+-------------------+--------------------------------------------------------------------+
|Detection          |Actions to determine areas of the image that contain vehicle or LP. |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+
|TrackEngine        |TrackEngine is library that used to track the position of the       |
|                   |                                                                    |
|                   |vehicle or LP in the sequence of frames and select the best frame.  |
+-------------------+--------------------------------------------------------------------+

Introduction
-----------------

This document contains general description of the LUNA CARS Stream 1.0.6 service.

The document is intended to explain to the user basic functionality of the service for detecting and tracking vehicle and license plate.

General information
--------------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS Analytics, CARS API and CARS Stream.

**VisionLabs LUNA CARS Stream** designed for detection and tracking of vehicles and LP in a video stream or detection in images. The main functions of the service are presented below:

* Video stream processing;

* Detection and tracking of vehicles and license plates;

* Choosing the best shot;

* Display of results of detection and recognition.

System description of CARS Stream
-------------------------------------------

CARS Stream processes the incoming video stream and searches for vehicles and license plate. The best shot is selected for each detected vehicle, after which the image of the vehicle and its license plate are sent to CARS API for recognition.

The general scheme of CARS Stream operation is shown in Figure 1
 
1. CARS Stream receives an incoming video stream.

2. The incoming video stream is divided into frames in CARS Stream.

3. Frames are sequentially transmitted to the TrackEngine. «TrackEngine» library implements a set of algorithms designed to track the position of one object in a sequence of frames. A separate strategy (Vehicle strategy) is responsible for vehicle detection and tracking, which is configured via a configuration file. TrackEngine contains a list of active tracking and the history of vehicle detections since start track.

4. On incoming frames TrackEngine sequentially launches full detect every 3 frames. The forecast of the vehicle position between detections is provided by the tracking mechanism (calman tracker).

5. TrackEngine buffers incoming frames (buffer size can be set in the «TrackEngine.conf» configuration file, by default 100 frames), as frames are displaced from the buffer, the algorithm for determining the best shot checks if the displaced frame contains a detection of a vehicle that is not blocked by other vehicles and at the same time the detection size is larger than other detections associated with tracking. In this case, the current detection is optimal. In the vehicle recognition area, the identification of the license plate is started. If the license plate is found, the images of the best shot of the car and the LP are sent to CARS API via an HTTP-request.

6. CARS API receives an HTTP request containing a car/license plate pair and runs the following algorithms:

* Recognition of vehicle’s license plate and country of registration;

* Vehicle type and category recognition;

* Vehicle brand and model recognition;

* Launching verification of vehicle's belonging to emergency vehicles or public transport.

7. CARS API sends the recognition results in JSON format to the body of the http request and writes them to a text file suitable for subsequent processing.

8. In the visual mode CARS Stream displays the detection results in the form of a bbox in the interface window with reference to track numbers. In the right part of the screen meta information is displayed with the recognition results obtained from CARS API. The interface window is used only in local operation mode, and the web interface is used in server mode.

9. Data is sent from CARS Stream to CARS Analytics using an HTTP-request.

Server and Local Operation Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The system can operate both in server and local mode. Unlike the local mode (video streams, video files and images are available), the server mode can only operate with video streams. At the same time, the server mode has several advantages:

* Creation, deletion and dynamic change of processed video streams. (Also allows you to configure video streams);

* Viewing video streams in real time;

* Obtaining information for all video streams or for each stream separately.